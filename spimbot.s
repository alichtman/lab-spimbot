# syscall constants
PRINT_STRING            = 4
PRINT_CHAR              = 11
PRINT_INT               = 1

# debug constants
PRINT_INT_ADDR              = 0xffff0080
PRINT_FLOAT_ADDR            = 0xffff0084
PRINT_HEX_ADDR              = 0xffff0088

# spimbot memory-mapped I/O
VELOCITY                    = 0xffff0010
ANGLE                       = 0xffff0014
ANGLE_CONTROL               = 0xffff0018
BOT_X                       = 0xffff0020
BOT_Y                       = 0xffff0024
OTHER_BOT_X                 = 0xffff00a0
OTHER_BOT_Y                 = 0xffff00a4
TIMER                       = 0xffff001c
SCORES_REQUEST              = 0xffff1018

ASTEROID_MAP                = 0xffff0050
COLLECT_ASTEROID            = 0xffff00c8

STATION_LOC                 = 0xffff0054
DROPOFF_ASTEROID            = 0xffff005c

GET_ENERGY                  = 0xffff00c0
GET_CARGO                   = 0xffff00c4

REQUEST_PUZZLE              = 0xffff00d0
SUBMIT_SOLUTION             = 0xffff00d4

THROW_PUZZLE                = 0xffff00e0
UNFREEZE_BOT                = 0xffff00e8
CHECK_OTHER_FROZEN          = 0xffff101c

# interrupt constants
BONK_INT_MASK               = 0x1000
BONK_ACK                    = 0xffff0060

TIMER_INT_MASK              = 0x8000
TIMER_ACK                   = 0xffff006c

REQUEST_PUZZLE_INT_MASK     = 0x800
REQUEST_PUZZLE_ACK          = 0xffff00d8

STATION_ENTER_INT_MASK      = 0x400
STATION_ENTER_ACK           = 0xffff0058

STATION_EXIT_INT_MASK       = 0x2000
STATION_EXIT_ACK            = 0xffff0064

BOT_FREEZE_INT_MASK         = 0x4000
BOT_FREEZE_ACK              = 0xffff00e4

# AARON Coord Masks
# 0x00XX00YY
ASTR_X_MASK 			= 0x00ff0000
ASTR_Y_MASK 			= 0x000000ff


.data
# put your data things here

.align 2
asteroid_map: .space 404

puzzle_data: .space 328

solution_data: .word 2			counts
counts: .space 8

.text
main:
    # put your code here :)
    li	$t4, BONK_INT_MASK					# bonk interrupt enable bit
	or	$t4, $t4, STATION_ENTER_INT_MASK	# station enter interrupt bit
	or	$t4, $t4, STATION_EXIT_INT_MASK		# station exit interrupt bit
	or  $t4, $t4, REQUEST_PUZZLE_INT_MASK	# request puzzle interrupt bit
	or  $t4, $t4, BOT_FREEZE_INT_MASK
	or	$t4, $t4, 1							# global interrupt enable
	mtc0	$t4, $12						# set interrupt mask (Status register)

	lw $t4, BOT_X 							# get bot x
	slt $t0, $t4, 200						# if bot too far left, don't request puzzle
	lw $t5, GET_ENERGY						# get bot energy
	slt $t1, $t5, 500
	and $t1, $t0, $t1
	beq $t1, 1, supernova_begone
	bgt $t5, 800, solve_complete			# if bot energy over 1000, don't request puzzle		
	srl $t4, $s8, 1
	beq $t4, 1, disjoint_call				# if puzzle to solve exists, don't request
	beq $s6, 1, solve_complete				# if puzzle has been requested, don't solve				
    la $s0, puzzle_data						# request puzzle
	sw $s0, REQUEST_PUZZLE   				
	li $s6, 1								# puzzle requested

solve_complete:
	la $s5, asteroid_map  					# getting asteroid map
    sw $s5, ASTEROID_MAP       				

    li $t3, 1								# $t3 = 1 (TRUE FOR COLLECT)
    sw $t3, COLLECT_ASTEROID				# enable collect asteroids

    sw $t3, ANGLE_CONTROL					# enable angle to be set

    lw $t7, BOT_X							# get bot's x coordinate
	lw $t4, BOT_Y							# get bot's y coordinate

	lw $t5, GET_ENERGY	
	ble $t5, 400, main
	and $t5, $s8, 1
	beq $t5, $0, locate_asteroid			# station exited, locate asteroids

	lw $t2, GET_CARGO						# get cargo amount
	li $t3, 60								# $t3 = 60 (CARGO CHECK AMOUNT)
	blt $t2, $t3, locate_asteroid			# if cargo amount < 60, locate asteroids

	li $t3, 1
	sw $t3, DROPOFF_ASTEROID				# enable dropping off asteroids
	j to_station							# to_station

disjoint_call:
	add $a0, $s0, 16						# lines
	move $a1, $s0							# canvas
	la $a2, solution_data

	jal count_disjoint_regions
	la $s2, solution_data
	lw $s2, 4($s2)

	lw $t5, GET_ENERGY	
	blt $t5, 500, just_submit

	srl $t1, $s8, 2
	beq $t1, 0, just_submit
	sw $s2, UNFREEZE_BOT
	and $s8, $s8, 4							# unset puzzle request
	j solve_complete

	lw $t5, CHECK_OTHER_FROZEN
	beq $t5, 1, just_submit

	li $t5, 1
	sw $t5, THROW_PUZZLE 

just_submit:
	sw $s2, SUBMIT_SOLUTION
	and $s8, $s8, 5							# unset puzzle request
	li $s6, 0
	j solve_complete

locate_asteroid:
	la $s5, asteroid_map  					# getting asteroid map
    sw $s5, ASTEROID_MAP    
	# lw $t3, 4($s5)							# get word for shorts x and y for first asteroid
    # and $t2, $t3, 0x00ff					# bitmask word with 0000 0000 1111 1111 to get y
    # srl $t3, $t3, 16						# shift word right 16 bits to get x
    move $a0, $s5
    beq $s3, 1, keep_moving
    jal find_asteroid_efficiently
    beq $v0, 5, main
    li $s3, 1
    move $t2, $v1
    move $t3, $v0

keep_moving:
    lw $t1, BOT_X							# get bot's x coordinate
    ble $t1, 60, supernova_begone
	lw $t4, BOT_Y							# get bot's y coordinate
	beq $t4, $t2, at_y						# if bot's y coordinate equals asteroid's y coordinate, go to at_y
    bgt $t2, $t4, asteroid_below			# if asteroid y is greater than bot y, go to asteroid_below

    bge $t1, 290, reg_angle_up
    li $t5, 315								# orient bot up
    li $t6, 10								# set velocity to 10
    j set_angle								# go to set angle

reg_angle_up:
	li $t5, 270								# orient bot up
    li $t6, 10								# set velocity to 10
    j set_angle								# go to set angle

reg_angle_down:
	li $t5, 90								# orient bot up
    li $t6, 10								# set velocity to 10
    j set_angle								# go to set angle

supernova_begone:
	li $t4, 0								# store 0 in $t4 for angle
	sw $t4, ANGLE 							# stores angle to 0 degrees

	li $t4, 1								# store 1 in $t4 for angle control
	sw $t4, ANGLE_CONTROL					# sets angle to 0 degrees

	li $t4, 10								# store 10 in $t4 for velocity
	sw $t4, VELOCITY 						# velocity = + 10

	lw $t4, BOT_X

	bge $t4, 150, locate_asteroid

	j supernova_begone

asteroid_below:
	bge $t1, 290, reg_angle_down
	li $t5, 45								# orient bot down
	li $t6, 10								# set velocity to 10

set_angle:
	sw $t5, ANGLE 							# set angle

set_velocity:
    sw $t6, VELOCITY						# set velocity
    j main

at_y:
	lw $t7, BOT_X
	beq $t7, $t3, at_x						# if bot's x coordinate equals asteroid's x coordinate, go to at_x
	blt $t7, $t3, asteroid_right			# if bot's x coordinate is less than asteroid's x coordinate, go to asteroid_right
	
	li $t5, 180								# orient bot left
	li $t6, 2								# set velocity to 2
	j set_angle								# go to set angle

asteroid_right:
	li $t5, 0								# orient bot right
	li $t6, 10								# set velocity to 10
	j set_angle								# go to set angle

at_x: 
	li $s3, 0
	j locate_asteroid						# go to locate_asteroid

to_station:
	li $t3, 170
	lw $t4, BOT_Y
	beq $t3, $t4, at_station_y						
    bgt $t3, $t4, station_below
    li $t5, 315								# orient bot up
    li $t6, 10								# set velocity to 10
    j set_angle								# go to set angle

station_below:
	li $t5, 45								# orient bot down
	li $t6, 10								# set velocity to 10
	j set_angle

at_station_y:
	li $t3, 250
	beq $t7, $t3, at_station_x				# if bot's x coordinate equals asteroid's x coordinate, go to at_x
	blt $t7, $t3, station_right				# if bot's x coordinate is less than asteroid's x coordinate, go to asteroid_right
	
	li $t5, 180								# orient bot left
	li $t6, 10								# set velocity to 0 (gravity will pull left)
	j set_angle								# go to set angle
station_right:
	li $t5, 0								# orient bot right
	li $t6, 10								# set velocity to 10
	j set_angle								# go to set angle	
at_station_x:
	lw $t7, GET_CARGO
	beq $t7, 0, locate_asteroid
	li $t5, 0
	li $t6, 2
	j set_angle

.kdata										# interrupt handler data (separated just for readability)
chunkIH:	.space 8						# space for two registers
non_intrpt_str:	.asciiz "Non-interrupt exception\n"
unhandled_str:	.asciiz "Unhandled interrupt type\n"

.ktext 0x80000180
interrupt_handler:
.set noat
	move $k1, $at							# Save $at                               
.set at
	la $k0, chunkIH
	sw $a0, 0($k0)							# Get some free registers                  
	sw $a1, 4($k0)							# by storing them to a global variable     

	mfc0 $k0, $13							# Get Cause register                       
	srl $a0, $k0, 2                
	and	$a0, $a0, 0xf						# ExcCode field                            
	bne	$a0, 0, non_intrpt         

interrupt_dispatch:							# Interrupt:                             
	mfc0 $k0, $13							# Get Cause register, again                 
	beq	$k0, 0, done						# handled all outstanding interrupts     

	and	$a0, $k0, BONK_INT_MASK				# is there a bonk interrupt?                
	bne	$a0, 0, bonk_interrupt  

	and	$a0, $k0, STATION_ENTER_INT_MASK	# is there a station enter interrupt?                
	bne	$a0, 0, station_enter_interrupt  

	and	$a0, $k0, STATION_EXIT_INT_MASK		# is there a station exit interrupt?                
	bne	$a0, 0, station_exit_interrupt   

	and	$a0, $k0, REQUEST_PUZZLE_INT_MASK	# is there a request puzzle interrupt?                
	bne	$a0, 0, request_puzzle_interrupt   

	and $a0, $k0, BOT_FREEZE_INT_MASK
	bne $a0, 0, bot_is_frozen_interrupt

	li $v0, PRINT_STRING					# Unhandled interrupt types
	la $a0, unhandled_str
	syscall 
	j done

bonk_interrupt:
  	sw 	$a1, BONK_ACK   					# acknowledge interrupt
  	# do stuff
  	j interrupt_dispatch       				# see if other interrupts are waiting

station_enter_interrupt:
  	sw $a1, STATION_ENTER_ACK   			# acknowledge interrupt
  	or $s8, $s8, 1
  	# do stuff
  	j interrupt_dispatch       				# see if other interrupts are waiting

station_exit_interrupt:
  	sw $a1, STATION_EXIT_ACK  		 		# acknowledge interrupt
  	and $s8, $s8, 6
  	# do stuff
  	j interrupt_dispatch       				# see if other interrupts are waiting

request_puzzle_interrupt:
  	sw $a1, REQUEST_PUZZLE_ACK   			# acknowledge interrupt
  	or $s8, $s8, 2							# solve puzzle flag
  	# do stuff
  	j interrupt_dispatch       				# see if other interrupts are waiting

bot_is_frozen_interrupt:
	# how do i ack this
	sw $s0, BOT_FREEZE_ACK
	or $s8, $s8, 4
	j interrupt_dispatch

non_intrpt:									# was some non-interrupt
	li $v0, PRINT_STRING
	la $a0, non_intrpt_str
	syscall									# print out an error message
	# fall through to done

done:
	la $k0, chunkIH
	lw $a0, 0($k0)							# Restore saved registers
	lw $a1, 4($k0)
.set noat
	move $at, $k1							# Restore $at
.set at 
	eret

.text
.globl find_asteroid_efficiently
find_asteroid_efficiently:
	sub     $sp, $sp, 16
    sw      $ra, 0($sp)
    sw      $s2, 4($sp)
    sw      $s3, 8($sp)
    sw      $s4, 12($sp)
#	Asteroid curr_best = &asteroid_map[0];
#	int curr_best_points = curr_best.points;

	add $t0, $a0, 4		# &curr_best -> $t0
	lw  $t1, 4($t0)		# curr_best_points -> $t1

# int cargo_space_left = cargo_limit - get_cargo();

	li  $t2, 256						# cargo limit is 256
	lw  $t3, GET_CARGO					# get cargo value
	sub $t2, $t2, $t3					# cargo_space_left -> $t2

#	for (int i = 1; i < asteroid_map.size; i++) {

	li $t3, 0 		# i = 0 -> $t3

for_loop_astr_map:
	lw $t7, GET_ENERGY		
	ble $t7, 400, break_out_of_loop
	addi $t3, $t3, 1		# i++, i = 1 on first iteration
	lw 	 $t4, 0($a0)			# arr.size -> $t4
	bge  $t3, $t4, return	# i < asteroid_map.size

#	if (curr_best_points == cargo_space_left) {
#			return curr_best;
#	}

	beq $t2, $t1, return

#		Asteroid potential = asteroid_map[i];

	sll  $t4, $t3, 3		# count * 8 -> $t4
	addi $t4, $t4, 4		# $t4 + 4 to offset for int length
	add  $t4, $a0, $t4		# $s0 (asteroid_map) + offset. potential -> $t4
	lw   $t5, 4($t4)		# potential.points -> $t5

#		// Can't carry the asteroid we're looking at, skip
#		if (potential.points > cargo_space_left) {
#			continue;
#		}

	bge  $t5, $t2, for_loop_astr_map

#		else if (potential.points > curr_best_points) {
#			// Set new current best
#			curr_best = potential;
#			curr_best_points = curr_best.points;
#		}

	bge $t1, $t5, for_loop_astr_map	# if curr_best_pts > potential.pts, skip

	move $t0, $t4 	# curr_best = potential;
	move $t1, $t5 	# curr_best_points = potential.points;

    j for_loop_astr_map

break_out_of_loop:
	li $v0, 5
	lw      $ra, 0($sp)
    lw      $s2, 4($sp)
    lw      $s3, 8($sp)
    lw      $s4, 12($sp)
    add     $sp, $sp, 16
	jr $ra

return:
	# &curr_best in $t0, curr_best_points in $t1
	lw  $t3, 0($t0)				# load X and Y (0x00XX00YY) -> $t3
	and $s2, $t3, ASTR_X_MASK	# $s2 = 0x00XX0000
	srl $v0, $s2, 16			# astr.x (0x000000XX) -> $s2
	and $v1, $t3, ASTR_Y_MASK	# astr.y (0x000000YY) -> $s3
	move $s4, $t1				# astr.points -> $s4

    lw      $ra, 0($sp)
    lw      $s2, 4($sp)
    lw      $s3, 8($sp)
    lw      $s4, 12($sp)
    add     $sp, $sp, 16
	jr $ra

.text
## struct Canvas {
##     // Height and width of the canvas.
##     unsigned int height;
##     unsigned int width;
##     // The pattern to draw on the canvas.
##     unsigned char pattern;
##     // Each char* is null-terminated and has same length.
##     char** canvas;
## };

## void draw_line(unsigned int start_pos, unsigned int end_pos, Canvas* canvas) {
##     unsigned int width = canvas->width;
##     unsigned int step_size = 1;
##     // Check if the line is vertical.
##     if (end_pos - start_pos >= width) {
##         step_size = width;
##     }
##     // Update the canvas with the new line.
##     for (int pos = start_pos; pos != end_pos + step_size; pos += step_size) {
##         canvas->canvas[pos / width][pos % width] = canvas->pattern;
##     }
## }

.globl draw_line
draw_line:
        lw      $t0, 4($a2)     # t0 = width = canvas->width
        li      $t1, 1          # t1 = step_size = 1
        sub     $t2, $a1, $a0   # t2 = end_pos - start_pos
        blt     $t2, $t0, cont
        move    $t1, $t0        # step_size = width;
cont:
        move    $t3, $a0        # t3 = pos = start_pos
        add     $t4, $a1, $t1   # t4 = end_pos + step_size
        lw      $t5, 12($a2)    # t5 = &canvas->canvas
        lbu     $t6, 8($a2)     # t6 = canvas->pattern
for_loop:
        beq     $t3, $t4, end_for
        div     $t3, $t0        #
        mfhi    $t7             # t7 = pos % width
        mflo    $t8             # t8 = pos / width
        mul     $t9, $t8, 4		# t9 = pos/width*4
        add     $t9, $t9, $t5   # t9 = &canvas->canvas[pos / width]
        lw      $t9, 0($t9)     # t9 = canvas->canvas[pos / width]
        add     $t9, $t9, $t7
        sb      $t6, 0($t9)     # canvas->canvas[pos / width][pos % width] = canvas->pattern
        add     $t3, $t3, $t1   # pos += step_size
        j       for_loop
        
end_for:
        jr      $ra


.text

## struct Canvas {
##     // Height and width of the canvas.
##     unsigned int height;
##     unsigned int width;
##     // The pattern to draw on the canvas.
##     unsigned char pattern;
##     // Each char* is null-terminated and has same length.
##     char** canvas;
## };
##
## // Count the number of disjoint empty area in a given canvas.
## unsigned int count_disjoint_regions_step(unsigned char marker,
##                                          Canvas* canvas) {
##     unsigned int region_count = 0;
##     for (unsigned int row = 0; row < canvas->height; row++) {
##         for (unsigned int col = 0; col < canvas->width; col++) {
##             unsigned char curr_char = canvas->canvas[row][col];
##             if (curr_char != canvas->pattern && curr_char != marker) {
##                 region_count ++;
##                 flood_fill(row, col, marker, canvas);
##             }
##         }
##     }
##     return region_count;
## }

.globl count_disjoint_regions_step
count_disjoint_regions_step:
        sub     $sp, $sp, 36
        sw      $ra, 0($sp)
        sw      $s0, 4($sp)
        sw      $s1, 8($sp)
        sw      $s2, 12($sp)
        sw      $s3, 16($sp)
        sw      $s4, 20($sp)
        sw      $s5, 24($sp)
        sw      $s6, 28($sp)
        sw      $s7, 32($sp)

        move    $s0, $a0
        move    $s1, $a1

        li      $s2, 0                  # $s2 = region_count
        li      $s3, 0                  # $s3 = row
        lw      $s4, 0($s1)             # $s4 = canvas->height
        lw      $s6, 4($s1)             # $s6 = canvas->width
        lw      $s7, 8($s1)             # canvas->pattern

cdrs_outer_for_loop:
        bge     $s3, $s4, cdrs_outer_end
        li      $s5, 0                  # $s5 = col

cdrs_inner_for_loop:
        bge     $s5, $s6, cdrs_inner_end
        lw      $t0, 12($s1)            # canvas->canvas
        mul     $t5, $s3, 4             # row * 4
        add     $t5, $t0, $t5           # &canvas->canvas[row]
        lw      $t0, 0($t5)             # canvas->canvas[row] 
        add     $t0, $t0, $s5           # &canvas->canvas[row][col]
        lbu     $t0, 0($t0)             # $t0 = canvas->canvas[row][col]
        beq     $t0, $s7, cdrs_skip_if  # curr_char != canvas->pattern
        beq     $t0, $s0, cdrs_skip_if  # curr_char != canvas->marker
        add     $s2, $s2, 1             # region_count++
        move    $a0, $s3
        move    $a1, $s5
        move    $a2, $s0
        move    $a3, $s1
        jal     flood_fill

cdrs_skip_if:
        add     $s5, $s5, 1             # col++
        j       cdrs_inner_for_loop

cdrs_inner_end:
        add     $s3, $s3, 1             # row++
        j       cdrs_outer_for_loop

cdrs_outer_end:
        move    $v0, $s2
        lw      $ra, 0($sp)
        lw      $s0, 4($sp)
        lw      $s1, 8($sp)
        lw      $s2, 12($sp)
        lw      $s3, 16($sp)
        lw      $s4, 20($sp)
        lw      $s5, 24($sp)
        lw      $s6, 28($sp)
        lw      $s7, 32($sp)
        add     $sp, $sp, 36
        jr      $ra

.text
## void count_disjoint_regions(const Lines* lines, Canvas* canvas,
##                             Solution* solution) {
##     // Iterate through each step.
##     for (unsigned int i = 0; i < lines->num_lines; i++) {
##         unsigned int start_pos = lines->coords[0][i];
##         unsigned int end_pos = lines->coords[1][i];
##         // Draw line on canvas.
##         draw_line(start_pos, end_pos, canvas);
##         // Run flood fill algorithm on the updated canvas.
##         // In each even iteration, fill with marker 'A', otherwise use 'B'.
##         unsigned int count = count_disjoint_regions_step('A' + (i % 2),
##                                                          canvas);
##         // Update the solution struct. Memory for counts is preallocated.
##         solution->counts[i] = count;
##     }
## }

.globl count_disjoint_regions
count_disjoint_regions:
	sub     $sp, $sp, 36
	sw      $ra, 0($sp)
	sw      $s0, 4($sp)
	sw      $s1, 8($sp)
	sw      $s2, 12($sp)
	sw      $s3, 16($sp)
	sw      $s4, 20($sp)
	sw      $s5, 24($sp)
	sw      $s6, 28($sp)
	sw      $s7, 32($sp)
	move    $s0, $a0        # s0 = lines
	move    $s1, $a1        # s1 = canvas
	move    $s2, $a2        # s2 = solution

	lw      $s4, 0($s0)     # s4 = lines->num_lines
	li      $s5, 0          # s5 = i
	lw      $s6, 4($s0)     # s6 = lines->coords[0]
	lw      $s7, 8($s0)     # s7 = lines->coords[1]

dr_for_loop:
	bgeu    $s5, $s4, dr_end_for
	mul     $t2, $s5, 4     # t2 = i*4
	add     $t3, $s6, $t2   # t3 = &lines->coords[0][i]
	lw      $a0, 0($t3)     # a0 = start_pos = lines->coords[0][i]
	add     $t4, $s7, $t2   # t4 = &lines->coords[1][i]
	lw      $a1, 0($t4)     # a1 = end_pos = lines->coords[1][i]
	move    $a2, $s1
	jal     draw_line

	li      $t9, 2
	div     $s5, $t9
	mfhi    $t6             # t6 = i % 2
	addi    $a0, $t6, 65    # a0 = 'A' + (i % 2)
	move    $a1, $s1        # count_disjoint_regions_step('A' + (i % 2), canvas)
	jal     count_disjoint_regions_step                # v0 = count
	lw      $t6, 4($s2)     # t6 = solution->counts
	mul     $t7, $s5, 4
	add     $t7, $t7, $t6   # t7 = &solution->counts[i]
	sw      $v0, 0($t7)     # solution->counts[i] = count
	addi    $s5, $s5, 1     # i++
	j       dr_for_loop

dr_end_for:
	lw      $ra, 0($sp)
	lw      $s0, 4($sp)
	lw      $s1, 8($sp)
	lw      $s2, 12($sp)
	lw      $s3, 16($sp)
	lw      $s4, 20($sp)
	lw      $s5, 24($sp)
	lw      $s6, 28($sp)
	lw      $s7, 32($sp)
	add     $sp, $sp, 36
	jr      $ra

.text

## struct Canvas {
##     // Height and width of the canvas.
##     unsigned int height;
##     unsigned int width;
##     // The pattern to draw on the canvas.
##     unsigned char pattern;
##     // Each char* is null-terminated and has same length.
##     char** canvas;
## };
## 
## // Mark an empty region as visited on the canvas using flood fill algorithm.
## void flood_fill(int row, int col, unsigned char marker, Canvas* canvas) {
##     // Check the current position is valid.
##     if (row < 0 || col < 0 ||
##         row >= canvas->height || col >= canvas->width) {
##         return;
##     }
##     unsigned char curr = canvas->canvas[row][col];
##     if (curr != canvas->pattern && curr != marker) {
##         // Mark the current pos as visited.
##         canvas->canvas[row][col] = marker;
##         // Flood fill four neighbors.
##         flood_fill(row - 1, col, marker, canvas);
##         flood_fill(row, col + 1, marker, canvas);
##         flood_fill(row + 1, col, marker, canvas);
##         flood_fill(row, col - 1, marker, canvas);
##     }
## }

.globl flood_fill
flood_fill:
        sub     $sp, $sp, 20
        sw      $ra, 0($sp)
        sw      $s0, 4($sp)
        sw      $s1, 8($sp)
        sw      $s2, 12($sp)
        sw      $s3, 16($sp)
        move    $s0, $a0                # $s0 = row
        move    $s1, $a1                # $s1 = col
        move    $s2, $a2                # $s2 = marker
        move    $s3, $a3                # $s3 = canvas
        blt     $s0, $0, ff_return      # row < 0
        blt     $s1, $0, ff_return      # col < 0
        lw      $t0, 0($s3)             # $t0 = canvas->height
        bge     $s0, $t0, ff_return     # row >= canvas->height
        lw      $t0, 4($s3)             # $t0 = canvas->width
        bge     $s1, $t0, ff_return     # col >= canvas->width 
        
        lw      $t0, 12($s3)            # canvas->canvas
        mul     $t1, $s0, 4
        add     $t0, $t1, $t0           # $t0 = &canvas->canvas[row]
        lw      $t0, 0($t0)             # canvas->canvas[row]
        add     $t1, $s1, $t0           # $t1 = &canvas->canvas[row][col]
        lbu     $t0, 0($t1)             # $t0 = curr = canvas->canvas[row][col]
        lbu     $t2, 8($s3)             # $t2 = canvas->pattern
        beq     $t0, $t2, ff_return     # curr == canvas->pattern
        beq     $t0, $s2, ff_return     # curr == marker

        sb      $s2, 0($t1)             # canvas->canvas[row][col] = marker
        sub     $a0, $s0, 1             # $a0 = row - 1
        jal     flood_fill              # flood_fill(row - 1, col, marker, canvas);
        move    $a0, $s0
        add     $a1, $s1, 1
        move    $a2, $s2
        move    $a3, $s3
        jal     flood_fill              # flood_fill(row, col + 1, marker, canvas);
        add     $a0, $s0, 1
        move    $a1, $s1
        move    $a2, $s2
        move    $a3, $s3
        jal     flood_fill              # flood_fill(row + 1, col, marker, canvas);
        move    $a0, $s0
        sub     $a1, $s1, 1
        move    $a2, $s2
        move    $a3, $s3
        jal     flood_fill              # flood_fill(row, col - 1, marker, canvas);

ff_return:
        lw      $ra, 0($sp)
        lw      $s0, 4($sp)
        lw      $s1, 8($sp)
        lw      $s2, 12($sp)
        lw      $s3, 16($sp)
        add     $sp, $sp, 20
        jr      $ra
