# debug constants
PRINT_INT_ADDR              = 0xffff0080
PRINT_FLOAT_ADDR            = 0xffff0084
PRINT_HEX_ADDR              = 0xffff0088

# spimbot memory-mapped I/O
VELOCITY                    = 0xffff0010
ANGLE                       = 0xffff0014
ANGLE_CONTROL               = 0xffff0018
BOT_X                       = 0xffff0020
BOT_Y                       = 0xffff0024
OTHER_BOT_X                 = 0xffff00a0
OTHER_BOT_Y                 = 0xffff00a4
TIMER                       = 0xffff001c
SCORES_REQUEST              = 0xffff1018

ASTEROID_MAP                = 0xffff0050
COLLECT_ASTEROID            = 0xffff00c8

STATION_LOC                 = 0xffff0054
DROPOFF_ASTEROID            = 0xffff005c

GET_ENERGY                  = 0xffff00c0
GET_CARGO                   = 0xffff00c4

REQUEST_PUZZLE              = 0xffff00d0
SUBMIT_SOLUTION             = 0xffff00d4

THROW_PUZZLE                = 0xffff00e0
UNFREEZE_BOT                = 0xffff00e8
CHECK_OTHER_FROZEN          = 0xffff101c

# interrupt constants
BONK_INT_MASK               = 0x1000
BONK_ACK                    = 0xffff0060

TIMER_INT_MASK              = 0x8000
TIMER_ACK                   = 0xffff006c

REQUEST_PUZZLE_INT_MASK     = 0x800
REQUEST_PUZZLE_ACK          = 0xffff00d8

STATION_ENTER_INT_MASK      = 0x400
STATION_ENTER_ACK           = 0xffff0058

STATION_EXIT_INT_MASK       = 0x2000
STATION_EXIT_ACK            = 0xffff0064

BOT_FREEZE_INT_MASK         = 0x4000
BOT_FREEZE_ACK              = 0xffff00e4

# AARON Coord Masks
# 0x00XX00YY
ASTR_X_MASK 			= 0x00ff0000
ASTR_Y_MASK 			= 0x000000ff

# 0xXXXXYYYY
STATION_X_MASK 			= 0xffff0000
STATION_Y_MASK 			= 0x0000ffff

# struct Puzzle{ # 16+12+300 = 318B
#     // Canvas
#     Canvas canvas;            # 12B
#     // Lines (at offset 16)
#     Lines lines;				# 16B
#     // The rest of the struct stores the actual data for canvas and lines.
#     // You should not need to know the exact format of this field.
#     // You should access data following the pointers in canvas and coords.
#     unsigned char[300] data;  # 300B
# }

# struct Canvas {       #12B
#	// Height and width of the canvas.
#	unsigned int height;	#4B
#	unsigned int width;		#4B
#	// The pattern to draw on the canvas.
#	unsigned char pattern;  #1B + 3B
#	// Each char* is null-terminated and has same length. char** canvas;
#};

# struct Lines {  #16B
# 	unsigned int num_lines;    # 4B + 4B
# 	// An int* array of size 2, where first element is an array of start pos // and second element is an array of end pos for each line.
#	unsigned int* coords[2];   # 8B
# };

# struct Asteroid -> 8B {
# 	short x;	2B
# 	short y;	2B
# 	int points;	4B
# };
#
# struct AsteroidMap -> 404B {
# 	// number of asteroids
# 	unsigned length;     			  4B
# 	// data for each asteroid
# 	struct Asteroid asteroids[50];	400B
# };


######
## NOTES
######

# ***Double check my size calculations on the Puzzle struct.***
# I wrote a test file for this, but idk why Lines is at offset 16.
# Run it with: $ gcc test.c && a.out
# It'll print the size of Lines, Canvas and Puzzle.

# All the interrupt handlers I've written acknowledge the interrupt and then write to a global flag.
# station_here, bot_frozen, and puzzle_here are the three globals I've got rn.

### TODOS
#
# + Double check size calculations on the Puzzle struct
# + DONE: Write efficient searching algorithm for asteroid selection
# + Write puzzle solving algorithm (Lab 8)
# + Deal with frozen bot

# int main()
# {
# 	request_puzzle();
#
# 	while (asteroid_map.size > 0) {
#
# 		// Find first asteroid and chase it
# 		Asteroid next_asteroid = find_asteroid_efficiently();
# 		check_for_puzzle();
# 		chase_and_collect_asteroid(next_asteroid);
#
#		 // Enough cargo to wait for station, go to rendevous point
#		 if (get_cargo() >= some_threshold) {
#			go_to_rendevous();
#			check_if_frozen();
#			check_for_puzzle();
#			wait_until_station_on_map();
#			deposit();
# 		} else {
# 			next_asteroid = find_asteroid_efficiently();
#			check_if_frozen();
#			chase_and_collect_asteroid(next_asteroid);
		# }
	# }
# }

.data
# put your data things here

# This align directive will make sure the space is aligned to 2^2 bytes
.align 2
station_here: .word 0 		# 1 if station here, 0 if not
bot_frozen: .word 0 		# 1 if frozen, 0 if not
puzzle_here: .word 0		# 1 if puzzle here, 0 if not
asteroid_map: .space 404	# space for asteroid_map struct
puzzle_data:  .space 344	# space for puzzle struct
# TODO: check space for puzzle

.text
main:
	# Expand stack and open up saved registers
    sub $sp, $sp, 36
    sw  $ra, 0($sp)
    sw  $s0, 4($sp)
    sw  $s1, 8($sp)
    sw  $s2, 12($sp)
    sw  $s3, 16($sp)
    sw  $s4, 20($sp)
    sw  $s5, 24($sp)
    sw  $s6, 28($sp)
    sw  $s7, 32($sp)

    # enable interrupts
	li	 $t1, TIMER_INT_MASK				# timer interrupt enable bit
	or	 $t1, $t1, BONK_INT_MASK			# bonk interrupt bit
	or   $t1, $t1, STATION_EXIT_INT_MASK  	# station exit interrupt
	or   $t1, $t1, STATION_ENTER_INT_MASK 	# station enter interrupt
	or   $t1, $t1, REQUEST_PUZZLE_INT_MASK  # request puzzle interrupt
	or   $t1, $t1, BOT_FREEZE_INT_MASK		# bot freeze interrupt
	or	 $t1, $t1, 1						# global interrupt enable
	mtc0 $t1, $12							# set interrupt mask (Status register)

	la  $s0, asteroid_map
	sw  $s0, ASTEROID_MAP 	# Populate asteroid_map with asteroid info
	la  $s7, puzzle_data
    sw  $s7, REQUEST_PUZZLE # Populate puzzle_data with puzzle

# 	while (True) {
#
# 		// Find first asteroid and chase it
# 		Asteroid next_asteroid = find_asteroid_efficiently();
# 		check_for_puzzle();
# 		chase_and_collect_asteroid(next_asteroid);

while_more_asteroids:

# TODO

# asteroid* find_asteroid_efficiently()
#{
#	// TODO: Get updated asteroid map
#	Asteroid curr_best = &asteroid_map[0];
#	int curr_best_points = curr_best.points;
#
#	int cargo_space_left = cargo_limit - get_cargo();
#
#	for (int i = 1; i < asteroid_map.size; ++i) {
#
#		// Highest pointing asteroid we can get is the curr_best, return
#		if (curr_best_points == cargo_space_left) {
#			return curr_best;
#		}
#
#		Asteroid potential = asteroid_map[i];
#
#		// Can't carry the asteroid we're looking at, skip
#		if (potential.points > cargo_space_left) {
#			continue;
#		}
#
#		// Validate curr_best coords to make sure they're safe
#		//		astr.x must be > 90 and < 270
#		//		astr.y must be > 30 and < 270
#		else if (potential.points > curr_best_points &&
#		         (potential.x >= 90 && potential.x <= 270 && potential.y >= 30 && potential.y <= 270)) {
#
#			// Set new current best
#			curr_best = potential;
#			curr_best_points = curr_best.points;
#		}
#	}
#
#	return curr_best;
#}

####
# Finds highest pointing asteroid that can be picked up. Places address in $t0
####
find_asteroid_efficiently:

#	Asteroid curr_best = &asteroid_map[0];
#	int curr_best_points = curr_best.points;

	add $t0, $s0, 4		# &curr_best -> $t0
	lw  $t1, 4($t0)		# curr_best_points -> $t1

# int cargo_space_left = cargo_limit - get_cargo();

	li  $t2, 256						# cargo limit is 256
	lw  $t3, GET_CARGO					# get cargo value
	sub $t2, $t2, $t3					# cargo_space_left -> $t2

#	for (int i = 1; i < asteroid_map.size; i++) {

	li $t3, 0 		# i = 0 -> $t3

for_loop_astr_map:

	addi $t3, $t3, 1		# i++, i = 1 on first iteration
	lw 	 $t4, 0($s0)			# arr.size -> $t4
	bge  $t3, $t4, return	# i < asteroid_map.size

#	if (curr_best_points == cargo_space_left) {
#			return curr_best;
#	}

	beq $t2, $t1, return

#		Asteroid potential = asteroid_map[i];

	sll  $t4, $t3, 3		# count * 8 -> $t4
	addi $t4, $t4, 4		# $t4 + 4 to offset for int length
	add  $t4, $s0, $t4		# $s0 (asteroid_map) + offset. potential -> $t4
	lw   $t5, 4($t4)		# potential.points -> $t5

#		// Can't carry the asteroid we're looking at, skip
#		if (potential.points > cargo_space_left) {
#			continue;
#		}

	bge  $t5, $t2, for_loop_astr_map

#		else if (potential.points > curr_best_points) {
#			// Set new current best
#			curr_best = potential;
#			curr_best_points = curr_best.points;
#		}

	bge $t1, $t5, for_loop_astr_map	# if curr_best_pts > potential.pts, skip

	move $t0, $t4 	# curr_best = potential;
	move $t1, $t5 	# curr_best_points = potential.points;

    j for_loop_astr_map

return:
	# &curr_best in $t0, curr_best_points in $t1
	lw  $t3, 0($t0)				# load X and Y (0x00XX00YY) -> $t3
	and $s2, $t3, ASTR_X_MASK	# $s2 = 0x00XX0000
	srl $s2, $s2, 16			# astr.x (0x000000XX) -> $s2
	and $s3, $t3, ASTR_Y_MASK	# astr.y (0x000000YY) -> $s3
	move $s4, $t1				# astr.points -> $s4
	j somewhere


###
# move_to_rendevous moves bot to (222, 98) and waits until the station is there to deposit the asteroids.
#
# TODO: Efficiency improvements -- use taylor.s to move at direct angle.
###
move_to_rendevous:
	# Station will be at (222, 98). Wait there and check coords over and over until deposit okay

	lw  $t0, BOT_X					# update BOT_X
	lw  $t1, BOT_Y					# update BOT_Y

	# FIX X
	li  $t2, 222						# 222 is target x
	bgt $t2, $t0, incr_bot_x_station	# if 222 > BOT_X, incr_bot_x
	blt $t2, $t0, decr_bot_x_station 	# if 222 < BOT_X, decr_bot_x

	# FIX Y
	li  $t2, 98							# 98 is target y
	bgt $t2, $t1, incr_bot_y_station 	# if station.y > BOT_Y, incr_y
	blt $t2, $t1, decr_bot_y_station	# if station.y < BOT_Y, decr_y

	# All coords match, deposit_asteroids
	j deposit_asteroids

incr_bot_x_station:
	# Increase BOT_X while chasing station
	li  $t2, 0				# Set ANGLE to face bot right
	sw  $t2, ANGLE
	li 	$t2, 1
	sw  $t2, ANGLE_CONTROL	# Set ABSOLUTE angle type
	li  $t2, 10
	sw  $t2, VELOCITY 		# Set VELOCITY to 10
	j move_to_rendevous

decr_bot_x_station:
	# Decrease BOT_X while chasing station
	li  $t2, 180			# Set ANGLE to face bot left
	sw  $t2, ANGLE
	li 	$t2, 1				# Set ABSOLUTE angle type
	sw  $t2, ANGLE_CONTROL
	li  $t2, 10
	sw  $t2, VELOCITY 		# Set VELOCITY to 10
	j move_to_rendevous

incr_bot_y_station:
	# Increase BOT_Y while chasing station
	li  $t2, 90				# Set ANGLE to face bot down
	sw  $t2, ANGLE
	li 	$t2, 1				# Set ABSOLUTE angle type
	sw  $t2, ANGLE_CONTROL
	li  $t2, 10
	sw  $t2, VELOCITY 		# Set VELOCITY to 10
	j move_to_rendevous

decr_bot_y_station:
	# Decrease BOT_Y while chasing station
	li  $t2, 270			# Set ANGLE to face bot up
	sw  $t2, ANGLE
	li 	$t2, 1				# Set ABSOLUTE angle type
	sw  $t2, ANGLE_CONTROL
	li  $t2, 10
	sw  $t2, VELOCITY 		# Set VELOCITY to 10
	j move_to_rendevous

deposit_asteroids:
	lw $t4, station_here
	beq $t4, $0, move_to_rendevous	# if station not here, move_to_rendevous

	# Get Station X and Y coords
	lw  $t7, STATION_LOC 			# station coords -> $t7
	and $s2, $t7, STATION_X_MASK
	srl $s2, $s2, 16				# Station X coords -> $s2
	and $s3, $t7, STATION_Y_MASK 	# Station Y coords -> $s3

	lw  $t0, BOT_X					# update BOT_X
	lw  $t1, BOT_Y					# update BOT_Y

	sub $t2, $t0, $s2			# botx - stationx -> $t2
	abs $t2, $t2				# abs(botx - stationx). Difference between xs
	li  $t3, 15
	bge $t2, $t3, move_to_rendevous	# if x is greate than 15 off, reposition

	sub $t2, $t1, $s3			# boty - stationy -> $t2
	abs $t2, $t2				# abs val of (boty - stationy). Difference between ys
	li  $t3, 15
	ble $t2, $t3, move_to_rendevous	# if y is less than 15 off, deposit_asteroids

	# Drop off asteroids
	li  $t3, 1
	sw  $t3, DROPOFF_ASTEROIDS
	j iterate_astroids_while

get_cargo_space_left:
	li  $t0, 256						# cargo limit is 256
	lw  $t1, GET_CARGO					# get cargo value
	sub $v0, $t0, $t1					# return 256 - current cargo value
	jr  $ra

done:
	# Load saved registers with original values and collapse stack.
    lw  $ra, 0($sp)
    lw  $s0, 4($sp)
    lw  $s1, 8($sp)
    lw  $s2, 12($sp)
    lw  $s3, 16($sp)
    lw  $s4, 20($sp)
    lw  $s5, 24($sp)
    lw  $s6, 28($sp)
    lw  $s7, 32($sp)
    add $sp, $sp, 36
    jr  $ra


###########
## INTERRUPT HANDLERS
###########

.kdata				# interrupt handler data (separated just for readability)
chunkIH:	.space 12	# space for three registers
non_intrpt_str:	.asciiz "Non-interrupt exception\n"
unhandled_str:	.asciiz "Unhandled interrupt type\n"

 .ktext 0x80000180
interrupt_handler:
 .set noat
 	move	$k1, $at		# Save $at
 .set at
	la	$k0, chunkIH
	sw	$a0, 0($k0)			# Get some free registers
	sw	$a1, 4($k0)			# by storing them to a global variable
	sw	$a2, 8($k0)			# by storing them to a global variable

	mfc0	$k0, $13		# Get Cause register
	srl	$a0, $k0, 2
	and	$a0, $a0, 0xf		# ExcCode field
	bne	$a0, 0, non_intrpt

interrupt_dispatch:					# Interrupt:
	mfc0	$k0, $13				# Get Cause register, again
	beq		$k0, 0, done_interrupt		# handled all outstanding interrupts

	and	$a0, $k0, BONK_INT_MASK		# is there a bonk interrupt?
	bne	$a0, 0, bonk_interrupt

	and	$a0, $k0, TIMER_INT_MASK	# is there a timer interrupt?
	bne	$a0, 0, timer_interrupt

	# add dispatch for other interrupt types here.

	and $a0, $k0, STATION_ENTER_INT_MASK # is there a station enter interrupt?
	bne $a0, 0, station_enter_interrupt

	and $a0, $k0, STATION_EXIT_INT_MASK # is there a station exit interrupt?
	bne $a0, 0, station_exit_interrupt

	and $a0, $k0, REQUEST_PUZZLE_INT_MASK  # request puzzle interrupt
	bne $a0, 0, request_puzzle_interrupt

	and $a0, $k0, BOT_FREEZE_INT_MASK # bot freeze interrupt
	bne $a0, 0, bot_freeze_interrupt

	li	$v0, PRINT_STRING	# Unhandled interrupt types
	la	$a0, unhandled_str
	syscall
	j	done_interrupt

station_enter_interrupt:

	sw   $a0, STATION_ENTER_ACK 	# acknowledge interrupt
	li 	 $a0, 1
	sw 	 $a0, station_here	# write TRUE to station_here
	j interrupt_dispatch

station_exit_interrupt:

	sw   $a0, STATION_EXIT_ACK	# acknowledge interrupt
	li 	 $a0, 0
	sw 	 $a0, station_here	# write FALSE to station_here
	j interrupt_dispatch

bonk_interrupt:

    sw  $a0, BONK_ACK   	# acknowledge interrupt
    li  $a0, 0				# Don't change bot direction
	sw  $a0, ANGLE
	li 	$a0, 0				# Set RELATIVE angle type
	sw  $a0, ANGLE_CONTROL
	li  $a0, -10
	sw  $a0, VELOCITY 		# Set VELOCITY to -10
    j   interrupt_dispatch       # see if other interrupts are waiting

timer_interrupt:
	sw	$a0, TIMER_ACK		# acknowledge interrupt

	# hover
	li  $a0, 180				# Set ANGLE to face bot right
	sw  $a0, ANGLE
	li 	$a0, 1					# Set ABSOLUTE angle type
	sw  $a0, ANGLE_CONTROL

	li  $a0, 0
	sw  $a0, VELOCITY 			# Set VELOCITY to 0

	lw	$a0, TIMER		# current time
	add	$a0, $a0, 100
	sw	$a0, TIMER		# request timer in 100 cycles

	j	interrupt_dispatch	# see if other interrupts are waiting

request_puzzle_interrupt:
	sw   $a0, REQUEST_PUZZLE_ACK 	# acknowledge interrupt
	li 	 $a0, 1
	sw 	 $a0, puzzle_here	# write TRUE to puzzle_here
	j	interrupt_dispatch	# see if other interrupts are waiting

bot_freeze_interrupt:
	sw   $a0, BOT_FREEZE_ACK 	# acknowledge interrupt
	li 	 $a0, 1
	sw 	 $a0, bot_frozen	# write TRUE to bot_frozen
	j	interrupt_dispatch	# see if other interrupts are waiting

non_intrpt:				# was some non-interrupt
	li	$v0, PRINT_STRING
	la	$a0, non_intrpt_str
	syscall				# print out an error message
	# fall through to done

done_interrupt:
	la	$k0, chunkIH
	lw	$a0, 0($k0)		# Restore saved registers
	lw	$a1, 4($k0)
	lw  $a2, 8($k0)
.set noat
	move	$at, $k1		# Restore $at
.set at
	eret
